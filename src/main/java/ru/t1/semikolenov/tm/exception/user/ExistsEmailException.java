package ru.t1.semikolenov.tm.exception.user;

import ru.t1.semikolenov.tm.exception.field.AbstractFieldException;

public final class ExistsEmailException extends AbstractFieldException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}
