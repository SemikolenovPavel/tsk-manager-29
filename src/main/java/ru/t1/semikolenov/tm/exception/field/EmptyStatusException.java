package ru.t1.semikolenov.tm.exception.field;

public final class EmptyStatusException extends AbstractFieldException {

    public EmptyStatusException() {
        super("Error! Status is empty...");
    }

}
