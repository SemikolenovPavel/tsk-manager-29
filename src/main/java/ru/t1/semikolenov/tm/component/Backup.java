package ru.t1.semikolenov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.command.data.DataBackupLoadCommand;
import ru.t1.semikolenov.tm.command.data.DataBackupSaveCommand;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    private void load() {
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    private void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            Thread.sleep(3000);
            save();
        }
    }

}
