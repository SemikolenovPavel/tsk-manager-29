package ru.t1.semikolenov.tm.command.data;

import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.dto.Domain;
import ru.t1.semikolenov.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public final class DataXmlLoadJaxbCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file.";

    @Nullable
    public static final String ARGUMENT = null;

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @Nullable
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(
                new Class[]{Domain.class},
                null);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
