package ru.t1.semikolenov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.model.ICommand;
import ru.t1.semikolenov.tm.api.service.IAuthService;
import ru.t1.semikolenov.tm.api.service.IServiceLocator;
import ru.t1.semikolenov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract String getArgument();

    public abstract void execute();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public String getUserId() {
        return getAuthService().getUserId();
    }

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull  final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @NotNull final String description = getDescription();
        @Nullable final String argument = getArgument();
        String result = "";
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}
